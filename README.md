# Практика с Монорепозиторием и пр.

```bash
npm init -y
npm i -D lerna
npx lerna init --packages="packages/*"
npx lerna create fe-ui -y
npx lerna create fe-macrocrm -y
npx lerna create be-macrocrm -y
npm i -D typescript
```

```bash
npm install --location=global create-single-spa
```

```bash
npm init vue@latest
```

```bash
# Добавить NX-болванку.
npx lerna add-caching
```

```bash
npx storybook@latest init
```